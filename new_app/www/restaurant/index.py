import frappe

def get_context(context):
    context.demo_name = frappe.get_doc('Demo','Student1')
    context.demo_list = frappe.db.get_list('Demo')
    context.demo_sub = frappe.db.get_list('Sub')
    return context
