import frappe

def get_context(context):
    # context.demo_list = frappe.db.get_list('Demo')
    # context.demo_list = frappe.db.sql("""select name from `tabDemo`order by creation ASC;""", as_dict = True)
    context.demo_list = frappe.db.sql("""select name from `tabDemo`;""", as_dict = True)
    # context.student_details = frappe.get_doc('Demo', frappe.form_dict.studentname)
    return context
