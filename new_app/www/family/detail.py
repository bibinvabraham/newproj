import frappe

def get_context(context):
    print(f"\n\n\n {frappe.form_dict.studentname} \n\n\n ")
    context.student_details = frappe.get_doc('Demo', frappe.form_dict.studentname)
    context.subjects = frappe.db.get_list('Subjects',{'parent':context.student_details.name},'subject_name')
    return context
