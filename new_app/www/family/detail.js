document.querySelector('#contact-student').addEventListener('click',
()=>{console.log('clicked')
  let student_name = document.querySelector('#student-name').value
  let student_mail = document.querySelector('#email').value
  let d = new frappe.ui.Dialog({
    title: 'Enter details',
    fields: [
        {
            label: 'Full Name',
            fieldname: 'full_name',
            fieldtype: 'Data'
        },
        {
            label: 'Email',
            fieldname: 'email',
            fieldtype: 'Data'
        },
        {
            label: 'Message',
            fieldname: 'message',
            fieldtype: 'Data'
        }
    ],
    primary_action_label: 'Submit',
    primary_action(values) {
        values.student_name = student_name;
        values.student_mail = student_mail;
        console.log(values);
        frappe.call({
          method: 'new_app.api.contact_student',
          args: values,
          callback: function(r){
            console.log(r)
          }
        })
        d.hide();
    }
});

d.show();
})
