// Copyright (c) 2016, Bibin and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Subject Selection"] = {
	"filters": [
		{
			'fieldname':'student',
			'label':__('Student'),
			'fieldtype':'Link',
			'options':'Demo',
		},
		{
			'fieldname':'specialisation',
			'label':__('Specialisation'),
			'fieldtype':'Link',
			'options':'Papper',
		},

	]
};
