# Copyright (c) 2013, Bibin and contributors
# For license information, please see license.txt
from __future__ import unicode_literals
from frappe import msgprint,_
import frappe

def execute(filters=None):
	columns, data = [], []
	columns = get_columns()
	doc = get_data(filters)

	for i in doc:
		row = frappe._dict({
		'name':i.name,
		'specialisation':i.specialisation,
		'subjects':i.subjects
		})
		data.append(row)
	return columns, data

def get_columns():
	return[{
	'fieldname':'name',
	'label':_('Student'),
	'fieldtype':'Data',
	'width':200
	},
	{
	'fieldname':'specialisation',
	'label':_('Specialisation'),
	'fieldtype':'Data',
	'width':200
	},

	]

def get_data(filters):
	conditions = get_conditions(filters)
	data = frappe.get_all(
	doctype = 'Demo',
	filters = conditions,
	fields = ['name','specialisation'],
	)
	
	return data


def get_conditions(filters):
	conditions = {}
	if filters.get('student'):
		conditions['student'] = filters.get('student')
		return conditions

	if filters.get('specialisation'):
		conditions['specialisation'] = filters.get('specialisation')
		return conditions
