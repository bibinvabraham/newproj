frappe.ready(function() {

  frappe.web_form.after_load = () => {
    frappe.msgprint('Please fill all values carefully');

    // takes a field value for evaluation
    frappe.web_form.on('email', (field, value) => {
      if (!value.includes('@')){
        frappe.throw(__('Please fill all values carefully'))
      }
    });

    frappe.web_form.validate = () => {
      let data = frappe.web_form.get_values();
      if (!data.email.includes('@')) {
            frappe.msgprint('Enter Valid email');
            return false;
        }
      return true
  };

}
})
