# Copyright (c) 2021, Bibin and contributors
# For license information, please see license.txt

import frappe
from twilio.rest import Client
from frappe.model.document import Document
from frappe.contacts.doctype.contact.contact import get_contact_name

class PersonalData(Document):
	def before_save(self):
		self.assigned_by = frappe.session.user

		# account_sid = 'ACee10f1760f7f8c57c942e8323106267b'
		# auth_token = '47f552cb698e5e1db436841c3a94f996'
		# client = Client(account_sid, auth_token)
		#
		# message = client.messages.create(
		#                               messaging_service_sid='MG1394c5e64bc9b53f5241630ca18584f3',
		#                               body='Hi Bibin',
		#                               to='+918281216433'
		#                           )
		# print(message.sid)

		user = frappe.session.user
		contact_name = get_contact_name('utharakannan123@gmail.com')
		contact = frappe.get_doc('Contact', contact_name)
		frappe.msgprint(f'Name is {contact_name}')
		party_doctype = contact.links[0].link_doctype
		party = contact.links[0].link_name
		party_data = frappe.get_doc(party_doctype, party)
		frappe.msgprint(f'{party_data}')
		print(f"\n\n {party_data} \n\n")
