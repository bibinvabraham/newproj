// Copyright (c) 2021, Bibin and contributors
// For license information, please see license.txt

frappe.ui.form.on('Personal Data', {
	refresh: function(frm) {

		// 		frm.add_custom_button(('Create Project'), function(){
		// 	frappe.msgprint(frm.doc.email);
		// },("Create Project"));

    frm.add_custom_button('New Project', ()=>{
      frappe.new_doc('Project',{
        'project_name':frm.doc.first_name
      })
    })

		frm.add_custom_button('New Customer', ()=>{
			// frappe.set_route("doctype","Project",{"Proje.ct":new-project-1})
			frappe.new_doc('Customer',{
				'customer_name':frm.doc.first_name,
			})
		})
	}
});
