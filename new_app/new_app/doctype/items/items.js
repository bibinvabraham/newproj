// Copyright (c) 2022, Bibin and contributors
// For license information, please see license.txt

frappe.ui.form.on('Items', {
	refresh: function(frm) {
    frm.add_custom_button('Get New Items', () =>{
      // frappe.msgprint('hlo')
      frappe.call({
        // method:"new_app.new_app.doctype.items.items.get_items",
        method:"get_items",
        doc:frm.doc
      })
    })
	}
});
