# Copyright (c) 2022, Bibin and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class Items(Document):
	@frappe.whitelist()
	def get_items(self):
		items = frappe.db.get_all('Item',['item_code','item_group','name','item_name'])
		for i in items:
			if not frappe.db.exists('Items',{'item_code':i.item_code}):
				barcode = frappe.db.get_value('Item Barcode',{'parent':i.name},['barcode'])
				doc = frappe.get_doc({
				"doctype":"Items",
				"item_code":i.item_code,
				"item_name":i.item_name,
				"item_group":i.item_group,
				"barcode":barcode,
				})
				doc.insert()
				frappe.msgprint('Updated')
