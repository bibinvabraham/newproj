frappe.pages['demo-chart'].on_page_load = function(wrapper) {
	new MyPage(wrapper)
}

MyPage = Class.extend({
	init: function(wrapper){
		this.page = frappe.ui.make_app_page({
			parent: wrapper,
			title: 'Mark List',
			single_column: true
	});
	this.dem();
},

dem: function(){
	let sub = function() {
		frappe.call({
			method:'new_app.new_app.page.demo_chart.demo_chart.get_sub',
			callback:function(r){
				// subs = r.message
				let subs = []
				let count = []
				r.message.forEach((item)=>{
					subs.push(item[0])
				})
				console.log(subs)

				frappe.call({
					method:'new_app.new_app.page.demo_chart.demo_chart.get_count',
					args:{subject:subs},
					callback:function(r){

						var val = r.message["sub"]
						var total = r.message["total"]
						console.log(val)
						console.log(total)

						val.forEach((item)=>{
							count.push(item)
						})
						count = count.reverse()
						console.log(count)
						let chart = new frappe.Chart( "#frost-chart", { // or DOM element
						data: {
						labels:subs,

						datasets: [
							{
								name: 'Subjects', chartType: 'bar',
								values: count
							},
						],

						yMarkers: [{ label: "Marker", value:total,
							options: { labelPos: 'left' }}],
						yRegions: [{ label: "Region", start: 0, end: total,
							options: { labelPos: 'right' }}]
						},

						title: "My Awesome Chart",
						type: 'axis-mixed', // or 'bar', 'line', 'pie', 'percentage'
						height: 300,
						colors: ['purple', '#ffa3ef', 'light-blue'],

						tooltipOptions: {
							formatTooltipX: d => (d + '').toUpperCase(),
							formatTooltipY: d => d + ' pts',
						}
						});
					}
				})
			}
		})
	}
	$(frappe.render_template(frappe.mark_list_page.body, this)).appendTo(this.page.main)
	sub();
}
})


let body = `
 <div class="widget-group">
				<div class="widget-group-head">
					<div class="widget-group-title h6 uppercase"></div>
					<div class="widget-group-control h6 text-muted"></div>
				</div>
				<div class="widget-group-body grid-col-3"><div class="widget  number-widget-box" data-widget-name="Total Declaration Submitted">
			<div class="widget-head">
				<div class="widget-title ellipsis"><div class="number-label">Total Declaration Submitted</div></div>
				<div class="widget-control"><div class="card-actions dropdown pull-right">
				<a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<button class="btn btn-default btn-xs"><span class="caret"></span></button>
				</a>
				<ul class="dropdown-menu" style="max-height: 300px; overflow-y: auto;">
					<li>
									<a data-action="action-refresh">Refresh</a>
								</li><li>
									<a data-action="action-edit">Edit</a>
								</li>
				</ul>
			</div></div>
			</div>
			<div class="widget-body"><div class="widget-content">
				<div class="number" style="color:undefined">0</div>
				<div class="card-stats grey-stat">
				<span class="percentage-stat">

					0  %
				</span>
				<span class="stat-period text-muted">
					since last month
				</span>
			</div></div></div>
		    <div class="widget-footer">
		    </div>
		</div><div class="widget  number-widget-box" data-widget-name="Total Salary Structure">
			<div class="widget-head">
				<div class="widget-title ellipsis"><div class="number-label">Total Salary Structure</div></div>
				<div class="widget-control"><div class="card-actions dropdown pull-right">
				<a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<button class="btn btn-default btn-xs"><span class="caret"></span></button>
				</a>
				<ul class="dropdown-menu" style="max-height: 300px; overflow-y: auto;">
					<li>
									<a data-action="action-refresh">Refresh</a>
								</li><li>
									<a data-action="action-edit">Edit</a>
								</li>
				</ul>
			</div></div>
			</div>
			<div class="widget-body"><div class="widget-content">
				<div class="number" style="color:undefined">12</div>
				<div class="card-stats grey-stat">
				<span class="percentage-stat">

					0  %
				</span>
				<span class="stat-period text-muted">
					since last month
				</span>
			</div></div></div>
		    <div class="widget-footer">
		    </div>
		</div><div class="widget  number-widget-box" data-widget-name="Total Incentive Given(Last month)">
			<div class="widget-head">
				<div class="widget-title ellipsis"><div class="number-label">Total Incentive Given(Last month)</div></div>
				<div class="widget-control"><div class="card-actions dropdown pull-right">
				<a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<button class="btn btn-default btn-xs"><span class="caret"></span></button>
				</a>
				<ul class="dropdown-menu" style="max-height: 300px; overflow-y: auto;">
					<li>
									<a data-action="action-refresh">Refresh</a>
								</li><li>
									<a data-action="action-edit">Edit</a>
								</li>
				</ul>
			</div></div>
			</div>
			<div class="widget-body"><div class="widget-content">
				<div class="number" style="color:undefined">₹ 0.00 </div>
				<div class="card-stats grey-stat">
				<span class="percentage-stat">

					0  %
				</span>
				<span class="stat-period text-muted">
					since last month
				</span>
			</div></div></div>
		    <div class="widget-footer">
		    </div>
		</div><div class="widget  number-widget-box" data-widget-name="Total Outgoing Salary(Last month)">
			<div class="widget-head">
				<div class="widget-title ellipsis"><div class="number-label">Total Outgoing Salary(Last month)</div></div>
				<div class="widget-control"><div class="card-actions dropdown pull-right">
				<a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<button class="btn btn-default btn-xs"><span class="caret"></span></button>
				</a>
				<ul class="dropdown-menu" style="max-height: 300px; overflow-y: auto;">
					<li>
									<a data-action="action-refresh">Refresh</a>
								</li><li>
									<a data-action="action-edit">Edit</a>
								</li>
				</ul>
			</div></div>
			</div>
			<div class="widget-body"><div class="widget-content">
				<div class="number" style="color:undefined">₹ 0.00 </div>
				<div class="card-stats grey-stat">
				<span class="percentage-stat">

					0  %
				</span>
				<span class="stat-period text-muted">
					since last month
				</span>
			</div></div></div>
		    <div class="widget-footer">
		    </div>
		</div></div>
			</div>
			<div id ="frost-chart"></id>
			`;
frappe.mark_list_page = {
	body: body
}
