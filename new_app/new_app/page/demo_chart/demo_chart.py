import frappe

@frappe.whitelist()
def get_sub():
    # frappe.msgprint('Hlo')
    subs = frappe.db.sql("select name from tabPapper;")
    return subs

@frappe.whitelist()
def get_count(subject):
    doc = frappe.db.get_list('Papper','name')
    sub = []
    detail = {}
    for i in range(len(doc)):
        sub.append(doc[i].name)
    count = []
    for i in range(len(sub)):
        c = frappe.db.count('Subjects',{'subject_name':sub[i]})
        count.append(c)
    detail["sub"] = count
    total = frappe.db.count('Subjects')
    detail["total"] = total
    return detail
