import frappe


@frappe.whitelist()
def contact_student(**args):
    doc = frappe.get_doc('Demo',args.get('student_name'))
    attachments = [frappe.attach_print(doc, doc.doctype, file_name = doc.name)]
    # frappe.sendmail([args.get('student_mail')],[args.get('message')],"Title", attachments = None)
    frappe.sendmail(recipients="bibincv237@gmail.com",
		subject="Test",
		message= "God please help me")
    return args


def sendmail(doc, recipients ,msg, title, attachments = None):
    email_args = {
    "recipients":recipients,
    "message": msg,
    "subject": title,
    "reference_doctype":doc.doctype,
    "reference_name":doc.name
    }

    frappe.enqueue(method=frappe.sendmail, **email_args)
