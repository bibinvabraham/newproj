from frappe import _

def get_data():
	return [
		{
			"module_name": "New App",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("New App")
		}
	]
