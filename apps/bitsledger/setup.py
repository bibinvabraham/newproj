from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in bitsledger/__init__.py
from bitsledger import __version__ as version

setup(
	name="bitsledger",
	version=version,
	description="App for Bitsledger",
	author="Faircode",
	author_email="info@faircodetech.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
