import frappe

def get_context(context):
    context.user = frappe.session.user
    if context.user != 'Guest':
        if context.user == 'Administrator':
            context.user_data = frappe.db.get_list('Partnership',['proposed_name_of_the_firm','name'])
        else:
            context.user_data = frappe.db.get_list('Partnership',['proposed_name_of_the_firm',"name"],{'owner':'bibin@faircodetech.com'})
    # context.user_data = frappe.db.get_list('Student Details',{'created_by':context.user},'first_name')
    return context
