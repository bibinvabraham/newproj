# Copyright (c) 2022, Faircode and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class StudentDetails(Document):
	def before_save(self):
		self.created_by = frappe.session.user
