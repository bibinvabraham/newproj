# Copyright (c) 2022, Faircode and contributors
# For license information, please see license.txt

import frappe
from frappe.website.website_generator import WebsiteGenerator

class Partnership(WebsiteGenerator):
	def before_save(self):
		new_lead = frappe.get_doc({
			"doctype":"Lead",
			"lead_name":self.proposed_name_of_the_firm,
			"mobile_no":self.phone_number,
			"email_id":self.email_id,
			"status":"Open"
		})

		new_lead.flags.ignore_permissions = True
		new_lead.insert()
		frappe.msgprint("Lead Added")

	def after_save(self):
		frappe.msgprint("Lead Added")
