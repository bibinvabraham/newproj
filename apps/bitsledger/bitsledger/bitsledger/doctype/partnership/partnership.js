// Copyright (c) 2022, Faircode and contributors
// For license information, please see license.txt

frappe.ui.form.on('Partnership', {
  refresh: function(frm) {
		// Custom button in saved documents of partnership doctype
			if(!frm.is_new()){
				frm.add_custom_button("New Customer", ()=>{
					frappe.new_doc('Customer',{ //Linking Customer Doctype
						'customer_name':frm.doc.proposed_name_of_the_firm,
					})
				})

				frm.add_custom_button("New Project", ()=>{
					frappe.new_doc('Project',{ // Linking with Project module
						'project_name':`Project for customer ${frm.doc.proposed_name_of_the_firm}`,
						'customer':frm.doc.proposed_name_of_the_firm // Customer is fetched only if it is saved
					})
				})
			}
		}

});
