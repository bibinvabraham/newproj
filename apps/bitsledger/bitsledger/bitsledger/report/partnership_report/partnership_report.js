// Copyright (c) 2016, Faircode and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Partnership Report"] = {
	"filters": [{
		'fieldname':'name',
		'label':__('Name'),
		'fieldtype':'Link',
		'options':'Partnership'
	},

	{
		'fieldname':'proposed_name_of_the_firm',
		'label':__('Proposed Name of the Firm'),
		'fieldtype':'Data',
	}

],

	// "formatter":function (row, cell, value, columnDef, dataContext, default_formatter) {
	// 	value = default_formatter(row, cell, value, columnDef, dataContext);
	// 	// if (dataContext.name > 0)
	// 	if (dataContext.name == 'PART0007') {
	// 		var $value = $value + $(value).css("background-color", "#75ff3a");
	// 		value = $value.wrap("<p></p>").parent().html();
	// 	}
	// 	return value;
	// }

	"formatter": function(value, row, column, data, default_formatter) {
        value = default_formatter(value, row, column, data);
        if(data.partnership_name == 'PART0007'){
            value = '<b style="color: red;">'+value+'</b>';
        }

				if(data.partnership_name == 'PART0006'){
            // value =  '<p style=margin:0px;padding-left:5px;background-color:red!important;>'+ value +</p>
						value = "<p style='margin:0px;padding-left:5px;background-color:yellow!important;'>"+value+"</p>";
        }

        return value;
    },
};
