# Copyright (c) 2013, Faircode and contributors
# For license information, please see license.txt

from __future__ import unicode_literals

from frappe import msgprint, _
import frappe

def execute(filters=None):
	columns, data = [], []
	columns = get_columns()
	d = get_partner(filters)

	for i in d:
		row = frappe._dict({
		'partnership_name':i.name,
		'proposed_name_of_the_firm':i.proposed_name_of_the_firm,
		'phone_number':i.phone_number
		})
		data.append(row)

	return columns, data


def get_columns():
	return[{
	'fieldname':'partnership_name',
	"label":_('Partnership Name'),
	'fieldtype':'Data',
	'width':300
	},
	{
	"fieldname": "proposed_name_of_the_firm",
	"label": _("Proposed Name of the Firm"),
	"fieldtype": "Data",
	'width':300
	},
	{
	"fieldname": "phone_number",
	"label": _("Phone Number"),
	"fieldtype": "Data",
	'width':300
	}]

def get_partner(filters):
	conditions = get_conditions(filters)
	data = frappe.get_all(
	doctype = 'Partnership',
	filters = conditions,
	fields = ['name','proposed_name_of_the_firm','phone_number']
	)
	return data

def get_conditions(filters):
	conditions={}
	if filters.get('name'):
		conditions['name'] = filters.get('name')
		return conditions

	if filters.get('proposed_name_of_the_firm'):
		conditions['proposed_name_of_the_firm']=filters.get('proposed_name_of_the_firm')
		return conditions
